import bcrypt, hashlib, sys

# python3 $.py password rounds

#bcrypt pypi 5b93c1726e50a93a033c36e5ca7fdcd29a5c7395af50a6892f5d9e7c6cfbfb29 bcrypt-3.2.0.tar.gz
#pip3 install --no-deps bcrypt-3.2.0.tar.gz

#>> working
#takes passwd,rounds. creates salt from sha256 of passwd. does bcrypt -> sha512 -> sha256

#>> important
#do not use plain bcrypt

passwd = sys.argv[1].encode('utf-8')
passwd_256_hash = hashlib.sha256(passwd).hexdigest()
salt_chars = str(passwd_256_hash) #sys.argv[2]
try:
    rounds_ = sys.argv[2]
except:
    rounds_ = '18'
if(int(rounds_) < 18):
    print ("rounds must be >= 18")
    sys.exit(0)

salt__final = ('$2a$' + rounds_ + '$' + salt_chars).encode('utf-8')

b_result = bcrypt.hashpw(passwd,salt__final).hex()
sha512_result = hashlib.sha512(b_result.encode('utf-8')).hexdigest()
sha_result = hashlib.sha256(sha512_result.encode('utf-8')).hexdigest()
print(sha_result)
