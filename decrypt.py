import subprocess, sys, os, ssl
from shutil import copyfile

if sys.version_info[0] >= 3:
    print("Must be using python2.7")
    sys.exit(0)

if(len(sys.argv) != 5):
    print("Usage python2.7 $.py in_file out_file times password")
    sys.exit(0)

in_file = sys.argv[1]
out_file = sys.argv[2]
times = int(sys.argv[3])
password = sys.argv[4]

copyfile(in_file, 'input_file') #src, dst

#finding openssl version
openssl_version = str(ssl.OPENSSL_VERSION)
if('OpenSSL 1.1.1' in openssl_version):
    print("having openssl version 1.1.1")
elif('OpenSSL 1.0.1' in openssl_version):
    print("having openssl version 1.0.1")

try:
    #for version 1.1.1 which are encrypted in 1.0.1
    for i in range(0,times):
        cmd = 'openssl aes-256-cbc -d -md md5 -in input_file -out ' + out_file + ' -pass pass:' + password + ' -base64'
        print (subprocess.check_output(cmd, shell = True))
        print ('times : ' , i+1)
        if(times > 1):
            copyfile(out_file, 'input_file')

    os.system('rm -f input_file')

    print("method 1 SUCCESS")
    sys.exit(0)
except Exception as e:
    print(e)
    print("\n---- method 1 failed ---- \n failed to decrypt with 1.1.1. trying with out '-md md5' option")

try:
    copyfile(in_file, 'input_file') #src, dst
    #for version 1.0.1
    for i in range(0,times):
        cmd = 'openssl aes-256-cbc -d -in input_file -out ' + out_file + ' -pass pass:' + password + ' -base64' 
        print (subprocess.check_output(cmd, shell = True))
        print ('times : ' , i+1)
        if(times > 1):
            copyfile(out_file, 'input_file')

    os.system('rm -f input_file')
    print("method 2 SUCCESS")
    sys.exit(0)
except Exception as e:
    print(e)
    print("\n---- method 2 failed ---- \n trying next method...")

try:

    #for version 1.1.1 which are encrypted in 1.1.1
    for i in range(0,times):
        cmd = 'openssl aes-256-cbc -d -md sha512 -pbkdf2 -iter 10000000 -salt -in input_file -out ' + out_file + ' -pass pass:' + password + ' -base64'
        print (subprocess.check_output(cmd, shell = True))
        print ('times : ', i+1)
        if(times > 1):
            copyfile(out_file, 'input_file')

    os.system('rm -f input_file')
    print("method 3 SUCCESS")

    sys.exit(0)
except Exception as e:
    print(e)
    print("\n\nfailed to decrypt with three methods :(")
