import subprocess, sys, ssl, os, binascii, random
from shutil import copyfile

#only works in openssl 1.1.1
openssl_version = str(ssl.OPENSSL_VERSION)
if('OpenSSL 1.1.1' not in openssl_version):
    print("only works in openssl version 1.1.1")
    sys.exit(0)

if(len(sys.argv) != 5):
    print("Usage python $.py in_file out_file times password")
    sys.exit(0)

print("---------- IMPORTANT ----------\n\t Pad small data like seed with random data\n")
shall_i_pad = raw_input("Shall i pad (y/n) : ")
if('y' in shall_i_pad):
    print('padding')
    pad = 1
elif('n' in shall_i_pad):
    print('not padding')
    pad = 0
else:
    print("wrong choice")
    sys.exit(0)

in_file = sys.argv[1]
out_file = sys.argv[2]
times = int(sys.argv[3])
password = sys.argv[4]

copyfile(in_file, 'input_file') #src, dst
if(pad):
    random_int1 = random.randint(1,100) + 10 #sometimes both are same
    random_int2 = random.randint(1,100)
    padding_start = str(binascii.b2a_hex(os.urandom(random_int1))) + '\n\n'

    padding_end = '\n\n' + str(binascii.b2a_hex(os.urandom(random_int2)))

    random_file = 'temp_file_' + str(binascii.b2a_hex(os.urandom(10)))
    padding_at_starting_cmd = "echo '" + padding_start + "' > " + random_file
    os.system(padding_at_starting_cmd)

    inserting_file_cmd = 'cat input_file >> ' + random_file
    os.system(inserting_file_cmd)

    padding_at_end_cmd = "echo '" + padding_end + "' >> " + random_file
    os.system(padding_at_end_cmd)

    move_temp_to_input_file_cmd = 'mv ' + random_file + ' input_file'
    os.system(move_temp_to_input_file_cmd)

for i in range(0,times):
    cmd = 'openssl aes-256-cbc -md sha512 -pbkdf2 -iter 10000000 -salt -in input_file -out ' + out_file + ' -pass pass:' + password + ' -base64'
    print subprocess.check_output(cmd, shell = True)
    print 'times : ' , i+1
    if(times > 1):
        copyfile(out_file, 'input_file')

os.system('rm -f input_file')


'''
#old code for openssl version 1.0.1
for i in range(0,times):
    cmd = 'openssl aes-256-cbc -in input_file -out ' + out_file + ' -pass pass:' + password + ' -base64' 
    print subprocess.check_output(cmd, shell = True)
    print 'times : ' , i+1
    if(times > 1):
        copyfile(out_file, 'input_file')

os.system('rm -f input_file')
'''
